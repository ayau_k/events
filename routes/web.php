<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* User routes */
Route::get('/login', [\App\Http\Controllers\UserController::class, 'indexLogin'])->name('login');
Route::post('/login', [\App\Http\Controllers\UserController::class, 'login']);
Route::post('/store', [\App\Http\Controllers\UserController::class, 'store'])->name('user_store');
Route::get('/logout', [\App\Http\Controllers\UserController::class, 'logout'])->name('logout');
Route::get('/register', [\App\Http\Controllers\UserController::class, 'register'])->name('register');

/* Event routes */
Route::get('/events/{page?}', [\App\Http\Controllers\EventController::class, 'index'])->middleware('auth')->name('events');
Route::get('/calendar', [\App\Http\Controllers\EventController::class, 'eventsCalendar'])->middleware('auth');

Route::group(['prefix' => 'event'], function () {
    Route::post('/', [\App\Http\Controllers\EventController::class, 'store']);
    Route::get('/{id}', [\App\Http\Controllers\EventController::class, 'edit']);
    Route::post('/delete/{id?}', [\App\Http\Controllers\EventController::class, 'delete']);
    Route::post('/update/{id?}', [\App\Http\Controllers\EventController::class, 'update']);

});
