<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Event
 * @package App\Models\Models
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $date
 * @property integer $user_id
 */
class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'date'
    ];
}
