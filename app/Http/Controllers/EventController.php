<?php

namespace App\Http\Controllers;

use App\Contracts\StatusCodes;
use App\Models\Models\Event;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    public function index($page = null)
    {
        /* @var User $user */
        $user  = Auth::user();

        $events = Event::where('user_id', $user->id)
            ->orderBy('updated_at', 'desc')
            ->paginate(5, '*', 'page', $page);

        return view('events', ['events' => $events]);
    }

    public function store(Request $request)
    {
        $data = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'date' => 'required',
        ])->validate();

        /* @var User $user */
        $user = Auth::user();

        $event = new Event();
        $event->user_id = $user->id;
        $event->fill($data);

        if ($event->save()) {
            return response()->json([
                'status' => 'success',
                'data' => $event
            ]);
        } else {
            return response([
                'status' => 'error',
                'message' => 'Could not save data'
            ], StatusCodes::HTTP_STATUS_UNPROCESSABLE_ENTITY);
        }
    }

    public function update($id, Request $request)
    {
        $data = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'date' => 'required'
        ])->validate();

        $event = Event::findOrFail($id);

        if ($event->update($data)) {
            return response()->json([
                'status' => 'success',
                'data' => $event
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Could not save data'
            ])->setStatusCode(StatusCodes::HTTP_STATUS_UNPROCESSABLE_ENTITY);
        }
    }

    public function edit($id)
    {
        $event = Event::findOrFail($id);

        return response()->json($event);
    }

    public function delete($id)
    {
        $event = Event::findOrFail($id);

        if ($event->delete()) {
            return response()->json([
                'status' => 'success'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Could not delete event'
            ])->setStatusCode(StatusCodes::HTTP_STATUS_UNPROCESSABLE_ENTITY);
        }
    }

    public function eventsCalendar(Request $request)
    {
        $data = Validator::make($request->all(), [
            'start' => 'required',
            'end' => 'required'
        ])->validate();

        /* @var User $user */
        $user = Auth::user();

        $events = Event::select('title', 'date')
            ->where('user_id', $user->id)
            ->where('date', '>=', $data['start'])
            ->where('date', '<=', $data['end'])
            ->get();

        return response()->json($events->toArray());
    }
}
