<?php

namespace App\Http\Controllers;

use App\Contracts\StatusCodes;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function indexLogin()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $data = Validator::make($request->all(), [
            'email' => 'required:string',
            'password' => 'required|string'
        ])->validate();

        $error = null;

        if (!Auth::attempt($data)) {
            $status = StatusCodes::HTTP_STATUS_UNAUTHORIZED;
            throw new \Exception('Auth failed', $status);
        }

        return response()->json([
            'status' => 'success'
        ]);
    }


    public function store(Request $request)
    {
        $data = Validator::make($request->all(), [
            'email' => 'required',
            'name' => 'required',
            'password' => 'required'
        ])->validate();

        try {
            $user = new User();
            $user->create($data);

            Auth::login($user);

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {

            Log::error($e);

            return response()->json([
                'status' => 'error',
                'message' => 'Неверные данные'
            ])->setStatusCode(StatusCodes::HTTP_STATUS_UNPROCESSABLE_ENTITY);
        }
    }

    public function register()
    {
        return view('register');
    }

    public function logout()
    {
        Auth::logout();

        return response()->redirectTo('/events');
    }
}
