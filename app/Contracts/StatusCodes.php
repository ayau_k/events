<?php
/**
 * Created by PhpStorm.
 * User: ayauzhan
 * Date: 2020-12-04
 * Time: 12:58
 */

namespace App\Contracts;


interface StatusCodes
{
    const HTTP_STATUS_SUCCESS = 200;
    const HTTP_STATUS_UNAUTHORIZED = 401;
    const HTTP_STATUS_UNPROCESSABLE_ENTITY = 422;
}