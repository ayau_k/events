$(document).ready(function () {
    $('#login').on('submit', function () {
        $.ajax({
            url: '/login',
            data: {
                'email': $('input[name=email]').val(),
                'password': $('input[name=password]').val(),
            },
            type: 'POST',
            success: function () {

                window.location.href = '/events';
            },
            error: function (result) {
                $.notify({
                    message: JSON.parse(result.responseText).message
                }, {
                    type: 'danger'
                });
            }
        });

        return false
    });

    $('#register').on('submit', function () {
        $.ajax({
            url: '/store',
            data: $(this).serialize(),
            type: 'POST',
            success: function () {
                window.location.href = '/events';
            },
            error: function (result) {
                $.notify({
                    message: JSON.parse(result.responseText).message
                }, {
                    type: 'danger'
                });
            }
        });

        return false
    });

    $('#createEventForm').on('submit', function () {
        var data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: '/event',
            data: data,
            success: function () {
                location.reload();
            },
            error: function (result) {
                $.notify({
                    message: JSON.parse(result.responseText).message
                }, {
                    type: 'danger'
                });
            }
        });

        return false;
    });

    $('.update-button').on('click', function () {
        var $id = $(this).data('value');
        $.ajax({
            type: 'GET',
            url: '/event/' + $id,
            success: function (result) {
                var $form = $('#updateEventForm');
                $form.data('value', $id);
                $form.find('textarea[name=description]').val(result['description']);
                $form.find('input[name=title]').val(result['title']);
                $form.find('input[name=date]').val(result['date']);
                $('#updateEventModal').modal('show');
            },
            error: function () {
                $.notify({
                    message: JSON.parse(result.responseText).message
                }, {
                    type: 'danger'
                });
            }
        });
    });

    $('#updateEventForm').on('submit', function () {
        var $id = $(this).data('value');
        $.ajax({
            data: $(this).serialize(),
            type: 'POST',
            url: '/event/update/' + $id,
            success: function () {
                location.reload();
            },
            error: function () {
                $.notify({
                    message: JSON.parse(result.responseText).message
                }, {
                    type: 'danger'
                });
            }
        });

        return false;
    });

    $('.logout-button').on('click', function () {
        var $id = $(this).data('value');
        $.ajax({
            data: $(this).serialize(),
            type: 'POST',
            url: '/event/delete/' + $id,
            success: function () {
                location.reload();
            },
            error: function () {
                $.notify({
                    message: JSON.parse(result.responseText).message
                }, {
                    type: 'danger'
                });
            }
        });

        return false;
    });

    $('#container').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listWeek'
        },
        defaultDate: new Date(),
        navLinks: true,
        editable: true,
        eventLimit: true,
        events: {
            url: '/calendar',
            method: 'GET',
            failure: function() {
                alert('there was an error while fetching events!');
            },
            color: 'yellow',
            textColor: 'black',
        }
    });
});