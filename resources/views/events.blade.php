@extends('general')
@section('content')
    <div class="container">
        <?php //dd($events) ?>
        <div class="row mt-3">
            <div class="col-5">
                <p class="mb-4">
                    Добавьте ваше мероприятие в календарь
                    <br>
                    <button type="button" class="btn btn-primary mt-3" data-toggle="modal"
                            data-target="#createEventModal">
                        Добавить
                    </button>
                </p>
                <div id="container" class="font-size-12 mt-4"></div>
            </div>
            <div class="col">
                <div class="tasks-list">
                    @foreach ($events as $event)
                        <div class="card mb-3 p-0">
                            <div class="card-header">
                                <div class="float-left">
                                    {{ $event['title'] }}
                                    <br>
                                    <small class="font-italic">{{ date('d.m.Y', strtotime($event->date)) }}</small>
                                </div>
                                <div class="float-right">
                                    <button class="update-button edit-button" data-value="{{ $event->id }}">
                                        <img class="image-20" src="{{ asset('/images/edit_icon.png') }}">
                                    </button>
                                    <button class="logout-button edit-button" data-value="{{ $event->id }}">
                                        <img class="image-20" src="{{ asset('/images/trash.png') }}">
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">{{ $event->description }}</p>
                            </div>
                            <div class="card-footer">
                                <small class="font-italic">
                                    {{ date('d.m.Y', strtotime($event->created_at)) }}
                                </small>
                            </div>
                        </div>
                    @endforeach
                </div>

                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item @if ($events->currentPage() == 1) disabled @endif">
                            <a class="page-link"
                               href="{{ route('events', ['page' => $events->currentPage() - 1]) }}"
                               aria-label="Previous"
                               @if ($events->currentPage() == 1) aria-disabled="true" @endif>
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        @for($i = 1; $i <= $events->lastPage(); $i++)
                            @if($i == $events->currentPage())
                                <li class="page-item active" aria-current="page">
                                    <a class="page-link" href="#">{{ $i }} <span class="sr-only">(current)</span></a>
                                </li>
                            @else
                                <li class="page-item">
                                    <a class="page-link" href="{{ route('events', ($i != 1) ? ['page' => $i] : []) }}">
                                        {{ $i }}
                                    </a>
                                </li>
                            @endif
                        @endfor
                        <li class="page-item  @if ($events->currentPage() == $events->lastPage()) disabled @endif">
                            <a class="page-link"
                               href="{{ route('events', ['page' => $events->currentPage() + 1]) }}"
                               aria-label="Next"
                               @if($events->currentPage() == $events->lastPage()) aria-disabled="true" @endif>
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createEventModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Мероприятие</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="" id="createEventForm">
                        <div class="form-group">
                            <label for="title">Название</label>
                            <input id="title" type="text" class="form-control" name="title"
                                   required>
                        </div>
                        <div>
                            <label for="datetime">Дата</label>
                            <input id="datetime" type="date" name="date">
                        </div>
                        <div class="form-group">
                            <label for="description">Детали</label>
                            <textarea id="description" type="text" class="form-control" name="description"
                                      required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="updateEventModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Редактировать</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="" id="updateEventForm">
                        <div class="form-group">
                            <label for="title">Название</label>
                            <input id="title" type="text" class="form-control" name="title"
                                   required>
                        </div>
                        <div>
                            <label for="datetime">Дата</label>
                            <input id="datetime" type="date" name="date">
                        </div>
                        <div class="form-group">
                            <label for="description">Детали</label>
                            <textarea id="description" type="text" class="form-control" name="description"
                                      required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection