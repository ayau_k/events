@extends('general')
@section('content')
    <div class="container">
        <div class="card mx-auto w-50 my-4">
            <div class="card-body">
                <form action="{{ route('user_store') }}" method="POST" id="register">
                    <div class="form-group">
                        <label for="name">Имя</label>
                        <input type="text" name="name" class="form-control" id="name" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Пароль</label>
                        <input type="password" name="password" class="form-control" id="password" required>
                    </div>

                    <button type="submit" class="btn btn-success">Зарегистрироваться</button>
                </form>
            </div>
        </div>
    </div>
@endsection