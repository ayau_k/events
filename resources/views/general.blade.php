<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Happy holidays!</title>
    <script src="{{ asset('js/jquery-3.3.1.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/scripts.js') }}" type="text/javascript"></script>
    <link type="text/css" href="{{ asset('/bootstrap-4.5.2-dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <script src="{{ asset('/bootstrap-4.5.2-dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/bootstrap-notify-3.1.3/bootstrap-notify.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/fullcalendar-3.10.2/lib/moment.min.js') }}"></script>
    <script src="{{ asset('/fullcalendar-3.10.2/fullcalendar.min.js') }}"></script>
    <link rel="stylesheet" href="{{ '/fullcalendar-3.10.2/fullcalendar.css' }}">
</head>
<body>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal">Holiday Events</h5>
    <nav class="my-2 my-md-0 mr-md-3">
    </nav>

    @if(\Illuminate\Support\Facades\Auth::check())
        <a href="{{ route('logout') }}" id="logoutButton" class="ml-4 btn btn-outline-primary">Выйти</a>
    @elseif(request()->path() == 'login')
        <a href="{{ route('register') }}" class="btn btn-outline-primary">
            Регистрация
        </a>
    @else
        <a href="{{ route('login') }}" class="btn btn-outline-primary mr-2" >
            Войти
        </a>
    @endif
</div>
<main>
    @yield('content')
</main>
</body>
</html>